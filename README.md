ali-delfani
===========

### About

This is a reposiroty of an excavator simulator, developed at RWTH university.

##### Instructions

The excavator can be controlled by following the simple steps as bellow:

1. Open the _simulator link_ from the menu on the left side of the screen on the dashboard.
2. Copy the **token** and paste it in the token field on your dashboard, then **press enter** to be connected to the broker.
3. Now you can control the different joints of the excavator using the sliders.

Enjoy!